//package com.project1.test;
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.when;
//
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.Transaction;
//import org.hibernate.engine.spi.SessionFactoryImplementor;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import com.project1.controller.UsersController;
//import com.project1.dao.UsersDao;
//import com.project1.model.UserRoles;
//import com.project1.model.Users;
//import com.project1.service.UsersService;
//import com.project1.util.HibernateUtil;
//
//
//public class UserDaoTest {
//
//	@Mock
//	HibernateUtil hUtil;
//	static SessionFactory sf;
//	SessionFactoryImplementor sfi;
//	
//	@Mock
//	Session s;
//	
//	@Mock
//	Transaction t;
//
//	@Mock
//	private Users user;
//	private UsersController testController;
//	private UserRoles testRole; 	
//	private Users testUser; 
//	private UsersService testService;
//		
//
//	@BeforeClass
//	public static void setUBeforeClass() throws Exception {
//		
//	}
//	
//	@AfterClass
//	public static void tearDownAfterClass() throws Exception {
//		if (sf != null)
//			sf.close();
//		
//	}
//
//	@Before
//	public void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		testService = new UsersService(new UsersDao(hUtil));
//		
//		testRole = new UserRoles(2, "employee");
//		testUser = new Users(23, "firstuser", "mypassword", "John", "Smith", "john.smith@fake.net", testRole);
//		when(hUtil.getSession()).thenReturn(s);
//		
//		when(s.createQuery("FROM testDao t WHERE firstname = " testDao.findByFirstname("John")))).thenReturn(testUser);
//		when(s.createQuery(testDao.findByUsername("firstuser"))).thenReturn(testUser);
//		when(s.createQuery(testDao.findByEmail("john.smith@fake.net"))).thenReturn(testUser);
//		when(s.get(UsersDao.class, 23)).thenReturn(testUser);
//		when(testDao.findByFirstname("John")).thenReturn(testUser);
//		when(testDao.findByEmail("john.smith@fake.net")).thenReturn(testUser);
//		when(testDao.findByLastname("Smith")).thenReturn(testUser);
//		when(testDao.findById(23)).thenReturn(testUser);
//		when(testDao.findByUsername("firstuser")).thenReturn(testUser);
//		
//	}
//
//
//	@After
//	public void tearDown() throws Exception {
//
//	}
//
//	@Test
//	public void testFindByFirstName() {
//		assertEquals(testDao.findByFirstname("John"), testUser);
//	}
//
//}
