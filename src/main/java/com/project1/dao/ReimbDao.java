package com.project1.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project1.MyLogger;
import com.project1.model.Reimbursements;
import com.project1.model.Users;
import com.project1.util.HibernateUtil;

public class ReimbDao implements GenericDao<Reimbursements> {
	
	private HibernateUtil hUtil;
	
	public final static Logger log = Logger.getLogger(ReimbDao.class);
	
	public ReimbDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}

	public ReimbDao() {
		// TODO Auto-generated constructor stub
	}


	@Override
	public Reimbursements findById(int Id) {
		Session ses = hUtil.getSession();

		Reimbursements reimb = ses.get(Reimbursements.class, Id);
		
		return reimb;
	}

	@Override
	public void update(Reimbursements entity) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();

		ses.merge(entity);
		tx.commit();
		MyLogger myLog = new MyLogger();
		
	}

	@Override
	public void insert(Reimbursements entity) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();

		ses.save(entity);
		tx.commit();
	
	}

//	@Override
//	public void delete(Reimbursements entity) {
//		// TODO Auto-generated method stub
//		
//	}
	
	public List<Reimbursements> findByStatus(int statusId) {
		Session ses = hUtil.getSession();

		List<Reimbursements> reimb = ses.createQuery("from Reimbursements where statusid = '" + statusId + "'", Reimbursements.class).getResultList();
		if(reimb==null) {
//			System.out.println("Could not find reimbursement: " + statusId + ".");
			return null;
		}

		 
	
		return reimb;
	}
	
	public List<Reimbursements> findPending() {
		Session ses = hUtil.getSession();

		List<Reimbursements> reimb = ses.createQuery("from Reimbursements where statusid = 1", Reimbursements.class).getResultList();
		if(reimb==null) {
//			System.out.println("There are no reimbursement requests pending");
			return null;
		}

		 
		
		return reimb;
	}
	
	public List<Reimbursements> findDeny() {
		Session ses = hUtil.getSession();

		List<Reimbursements> reimb = ses.createQuery("from Reimbursements where statusid = 3", Reimbursements.class).getResultList();
		if(reimb==null) {
//			System.out.println("There are no reimbursement requests pending");
			return null;
		}

		 
		
		return reimb;
	}
	
	public List<Reimbursements> findApproved() {
		Session ses = hUtil.getSession();

		List<Reimbursements> reimb = ses.createQuery("from Reimbursements where statusid = 2", Reimbursements.class).getResultList();
		if(reimb==null) {
//			System.out.println("There are no reimbursement requests pending");
			return null;
		}

		 
		
		return reimb;
	}
	
	public List<Reimbursements> findByAuthor(Users user) {
		Session ses =hUtil.getSession();
		int userId = user.getUserId();

		List<Reimbursements> reimb = ses.createQuery("from Reimbursements where author = '" + userId + "'", Reimbursements.class).getResultList();
		if(reimb==null) {
//			System.out.println("Could not find reimbursement: " + userId + ".");
			return null;
		}
		
	
		return reimb;	
	}
		
		public List<Reimbursements> getAll () {
			Session ses = hUtil.getSession();

			List<Reimbursements> reimb = ses.createQuery("from Reimbursements ", Reimbursements.class).getResultList();
	
		 
	
		return reimb;
	}

	@Override
	public void delete(Reimbursements entity) {
		// TODO Auto-generated method stub
		
	}

}
