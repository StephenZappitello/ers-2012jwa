package com.project1.dao;

import com.project1.model.Users;

public interface GenericDao <E> {
	
public E findById(int Id);
public void update(E entity);
public void insert(E entity);
public void delete(E entity);



}
