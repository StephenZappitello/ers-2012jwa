package com.project1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ERSDBConn {
	private String url = "jdbc:mariadb://rev-training.cda6ezmtod4p.us-east-2.rds.amazonaws.com:3306/ersdb";
	private String username = "ersuser";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}

}
