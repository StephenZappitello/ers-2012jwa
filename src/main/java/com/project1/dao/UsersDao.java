package com.project1.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project1.MyLogger;
import com.project1.model.Users;
import com.project1.util.HibernateUtil;
import org.apache.log4j.NDC;

public class UsersDao {
	public final static Logger log = Logger.getLogger(UsersDao.class);
	
	private static HibernateUtil hUtil;
	public UsersDao() {
		// TODO Auto-generated constructor stub
	}
	
	
	public UsersDao(HibernateUtil hUtil) {
		super();
		this.hUtil = hUtil;
	}


	public Users findById(int Id) {
		Session ses = hUtil.getSession();
		Users user = ses.createQuery("from Users where userid = '" + Id + "'", Users.class).uniqueResult();
		if(user==null) {
//			System.out.println("Could not find " + Id + ".");
			return null;
		}
//		System.out.println(user);
	
		return user;
	}

	public Users findByUsername(String username) {
		Session ses = hUtil.getSession();
		Users user = ses.createQuery("from Users where username = '" + username + "'", Users.class).uniqueResult();
		if(user==null) {
//			System.out.println("Could not find " + username + ".");
			return null;
		}
	
		
		return user;
	}

	public List<Users> findByFirstname(String firstname) {
		Session ses = hUtil.getSession();
		List<Users> user = ses.createQuery("from Users where firstname = '" + firstname + "'", Users.class).list();
		if(user.size()==0) {
//			System.out.println("Could not find " + firstname + ".");
			return null;
		}
	
		return user;
		
//		CriteriaBuilder builder = hUtil.getSession().getCriteriaBuilder();
//		CriteriaQuery<Users> crit = builder.createQuery(Users.class);
//		Root<Users> usersRoot = crit.from(Users.class);
//		crit.select(usersRoot).where(usersRoot.get(firstname));
//		TypedQuery<Users> query = hUtil.getSession().createQuery(crit);
//
//		hUtil.closeSes();
//		return query.getResultList();
	}

	public List<Users> findByLastname(String lastname) {
		Session ses = hUtil.getSession();
		List<Users> user = ses.createQuery("from Users where lastname = '" + lastname + "'", Users.class).list();
		if(user==null) {
//			System.out.println("Could not find " + lastname + ".");
			return null;
		}
		
		return user;
//		CriteriaBuilder builder = hUtil.getSession().getCriteriaBuilder();
//		CriteriaQuery<Users> crit = builder.createQuery(Users.class);
//		Root<Users> usersRoot = crit.from(Users.class);
//		crit.select(usersRoot).where(usersRoot.get(lastname));	
//		TypedQuery<Users> query = hUtil.getSession().createQuery(crit);
//		
//		hUtil.closeSes();
//		return query.getResultList();
	}
	
	public Users findByFullname(String firstname, String lastname) {
		Session ses = hUtil.getSession();
		Users user = ses.createQuery("from Users where firstname = '" + firstname + "'" + "AND lastname = '" + lastname + "'", Users.class).uniqueResult();
		if(user==null) {
//			System.out.println("Could not find " + firstname + ".");
			return null;
		}
	
		return user;
	}

	public Users findByEmail(String email) {
		Session ses = hUtil.getSession();
		Users user = ses.createQuery("from Users where email = '" + email + "'", Users.class).uniqueResult();
		if(user==null) {
//			System.out.println("Could not find " + email + ".");
			return null;
		}
	
		return user;
//		CriteriaBuilder builder = hUtil.getSession().getCriteriaBuilder();
//		CriteriaQuery<Users> crit = builder.createQuery(Users.class);
//		Root<Users> usersRoot = crit.from(Users.class);
//		crit.select(usersRoot).where(usersRoot.get(email));	
//		TypedQuery<Users> query = hUtil.getSession().createQuery(crit);
//		
//		hUtil.closeSes();
//		return query.getResultList();
	}
	
	public List<Users> getAll() {
		Session ses = hUtil.getSession();
		List<Users> user = ses.createQuery("from Users", Users.class).list();
		if(user.size()==0) {
//			System.out.println("Nothing here");
			return null;
		}
	
		return user;
	}

	public void insertUser(Users user) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();

		ses.save(user);
		tx.commit();
	
	}

	public void updateUser(Users user) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();

		ses.merge(user);
		tx.commit();

	}
	
	public void deleteUser(int id) {
		Session ses = hUtil.getSession();
		Transaction tx = ses.beginTransaction();
		Users user = findById(id);
		
		ses.delete(user);
	
	}


	public List<Users> findByUserRole(int userRole) {
		Session ses = hUtil.getSession();
		List<Users> user = ses.createQuery("from Users where roleId = '" + userRole + "'", Users.class).list();
		if(user==null) {
//			System.out.println("Could not find " + userRole + ".");
			return null;
		}
		return user;
	}


	public void closeSes() {
		hUtil.closeSes();
		
	}

}
