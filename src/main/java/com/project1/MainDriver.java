package com.project1;

import org.apache.log4j.Logger;

import com.project1.controller.ReimbController;
import com.project1.controller.UsersController;
import com.project1.dao.ReimbDao;
import com.project1.dao.UsersDao;
import com.project1.service.ReimbService;
import com.project1.service.UsersService;
import com.project1.util.HibernateUtil;

import io.javalin.Javalin;

public class MainDriver {
	
	

	
	public static void main(String[] args) {
		
		UsersController uCont = new UsersController(new UsersService(new UsersDao(new HibernateUtil())));
		ReimbController rCont = new ReimbController(new ReimbService(new ReimbDao(new HibernateUtil())));
		
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("/frontend");

		});
		
		app.start(9012);

		app.post("/user/login", uCont.login);
		app.get("/user/session", uCont.getSessUser);
		app.post("/user/new", uCont.newUser);
		app.post("/reimb/new", rCont.postReimb);
		app.put("/reimb/update", rCont.updateReimb);
		app.get("/reimb/author", rCont.getByAuthor);
		app.get("/reimb/pending", rCont.getByPending);
		app.get("/reimb/all", rCont.getAllReimb);
		app.post("/user/logout", uCont.logout);
		app.post("/reimb/byapprove", rCont.getByApproved);
		app.post("/reimb/bydeny", rCont.getByDeny);
		app.post("/reimb/approve", rCont.approve);
		app.post("/reimb/deny", rCont.deny);
//		app.get("/reimb/byid/:reimbId", rCont.findById);


		
		
		app.exception(NullPointerException.class, (e,ctx) ->{
			ctx.status(404);
			ctx.result("How embarassing!  There seems to be a error.");
		});
	
 

	}
}
