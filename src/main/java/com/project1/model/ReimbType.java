package com.project1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name ="reimbtype")

public class ReimbType {
	
	@Id
	@Column(name="typeid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int typeId;
	
	@Column(name="reimbtype")
	private String reimbType;
	
//	@OneToMany(mappedBy="Reimbursements")
//	private List<Reimbursements> reimb;

	public ReimbType() {
		// TODO Auto-generated constructor stub
	}
	

	public ReimbType(int typeId, String reimbType) {
		super();
		this.typeId = typeId;
		this.reimbType = reimbType;
	}

	// populating the table with the following values as these entries are not going to be changed by the client 
//	ReimbType lodging = new ReimbType(1, "LODGING");
//	ReimbType travel = new ReimbType(2, "TRAVEL");
//	ReimbType food = new ReimbType(3, "FOOD");
//	ReimbType other = new ReimbType(4, "OTHER");
	
	
	public ReimbType(int typeId) {
		super();
		switch(typeId) {
		case 1:
			this.typeId = typeId;
			this.reimbType = "LODGING";
			break;
		case 2:
			this.typeId = typeId;
			this.reimbType = "TRAVEL";
			break;
		case 3:
			this.typeId = typeId;
			this.reimbType = "FOOD";
			break;
		case 4:
			this.typeId = typeId;
			this.reimbType = "OTHER";
			break;
		}
	}


	//no setters since this is just a lookup table
	public int getTypeId() {
		return typeId;
	}
	
	public String getType() {
		return reimbType;
	}




	@Override
	public String toString() {
		return "ReimbType [typeId=" + typeId + ", type=" + reimbType + "]";
	}
	
	
}
