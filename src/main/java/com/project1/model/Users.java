package com.project1.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.ForeignKey;



@Entity
@Table (name = "users")

public class Users {
	
	//this is the primary key
	@Id
	@Column(name="userid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="email")
	private String email;

	//creating the foreign key constraint so that one user can have only one role, but a role can be mapped to many users
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="roleid", foreignKey = @ForeignKey(name = "ROLE_FK"))
//	@JoinColumn(name= "ROLE_FK")
	private UserRoles userRoleHolder;

	public Users() {
		// TODO Auto-generated constructor stub
	}

	public Users(String username, String password, String firstname, String lastname, String email,
			int roleId) {
		super();
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.userRoleHolder = new UserRoles(roleId);
	}

	public Users(int userId, String username, String password, String firstname, String lastname, String email,
			UserRoles userRole) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.userRoleHolder = userRole;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserRoles getUserRole() {
		return userRoleHolder;
	}

	public void setUserRole(UserRoles userRole) {
		this.userRoleHolder = userRole;
	}

	public int getUserId() {
		return userId;
	}

	@Override
	public String toString() {
		return "Users [userId=" + userId + ", username=" + username + ", password=" + password + ", firstname="
				+ firstname + ", lastname=" + lastname + ", email=" + email + ", userRole=" + userRoleHolder + "]";
	}

	public void sesClose() {
		// TODO Auto-generated method stub
		
	}
	
	

}
