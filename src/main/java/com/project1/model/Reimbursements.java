package com.project1.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table (name = "reimbursements")

public class Reimbursements {
	
	//primary key
	@Id
	@Column(name="reimbid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int reimbId;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="submitted")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date submitted;
	
	@Column(name="resolved")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date resolved;
	
	@Column(name="description")
	private String description;
	
	@Column(name="receipt")
	private Blob img;
	
	@OneToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="author", foreignKey = @ForeignKey(name = "AUTHOR_FK"))
	private Users author;
	
	@OneToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="resolver", foreignKey = @ForeignKey(name = "RESOLVER_FK"))
	private Users resolver;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="statusid", foreignKey = @ForeignKey(name = "STATUS_ID_FK"))
	private ReimbStatus statusHolder;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="typeid", foreignKey = @ForeignKey(name = "TYPE_ID_FK"))
	private ReimbType typeHolder;

	@PrePersist
	void updateDates() {
		if (submitted == null) {
			submitted = new Date();
		}
//		resolved = new Date();
	}
	
	
	public Reimbursements() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Reimbursements(double amount, String description, Blob img, Users author, ReimbType typeHolder) {
		super();
		this.amount = amount;
		this.resolved = null;
		this.description = description;
		this.img = img;
		this.author = author;
		this.resolver = null;
		this.statusHolder = new ReimbStatus(1);
		this.typeHolder = typeHolder;
	}
	
	public Reimbursements(double amount, String description, Users author, ReimbType typeHolder) {
		super();
		this.amount = amount;
		this.resolved = null;
		this.description = description;
		this.img = null;
		this.author = author;
		this.resolver = null;
		this.statusHolder = new ReimbStatus(1);
		this.typeHolder = typeHolder;
	}



	public Reimbursements(double amount, String description, Blob img,
			Users author, Users resolver, ReimbStatus statusHolder, ReimbType typeHolder) {
		super();
		this.amount = amount;
//		this.submitted = Timestamp.getMyTimestamp();
//		this.resolved = Timestamp.getMyTimestamp();
		this.description = description;
		this.img = img;
		this.author = author;
		this.resolver = resolver;
		this.statusHolder = statusHolder;
		this.typeHolder = typeHolder;
	}



	public int getReimbId() {
		return reimbId;
	}



	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}



	public double getAmount() {
		return amount;
	}



	public void setAmount(double amount) {
		this.amount = amount;
	}



	public Date getSubmitted() {
		return submitted;
	}



	public void setSubmitted(Date submitted) {
		this.submitted = submitted;
	}



	public Date getResolved() {
		return resolved;
	}



	public void setResolved(Date resolved) {
		this.resolved = resolved;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public Blob getImg() {
		return img;
	}



	public void setImg(Blob img) {
		this.img = img;
	}



	public Users getAuthor() {
		return author;
	}



	public void setAuthor(Users author) {
		this.author = author;
	}



	public Users getResolver() {
		return resolver;
	}



	public void setResolver(Users resolver) {
		this.resolver = resolver;
	}



	public ReimbStatus getStatusHolder() {
		return statusHolder;
	}



	public void setStatusHolder(ReimbStatus statusHolder) {
		this.statusHolder = statusHolder;
	}



	public ReimbType getTypeHolder() {
		return typeHolder;
	}



	public void setTypeHolder(ReimbType typeHolder) {
		this.typeHolder = typeHolder;
	}


		
		
	/*
	 * reimb_id NUMBER (25),
	amount NUMBER (25),
	submitted TIMESTAMP,
	resolved TIMESTAMP,
	description VARCHAR2(250),
	receiptimg BLOB,
	author NUMBER (25),
	resolver NUMBER (25),
	statusid NUMBER (25),
	typeid NUMBER (25),
	PRIMARY KEY (reimb_id),
	FOREIGN KEY (author) REFERENCES users (user_id),
	FOREIGN KEY (resolver) REFERENCES users (user_id),
	FOREIGN KEY (status_id) REFERENCES reimbursement_status (status_id),
	FOREIGN KEY (type_id) REFERENCES reimbursement_type (type_id)
	 */
	




}

