package com.project1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "userroles")

public class UserRoles {

		
		@Id
		@Column(name="roleid")
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int roleId;
		
		@Column(name="userrole")
		private String userRole;
		
//		@OneToMany(mappedBy="userRoleHolder")
//		private List<Users> users = new ArrayList<Users>();

		public UserRoles() {
			super();
			// TODO Auto-generated constructor stub
		}

//		public UserRoles(int roleId, String userRole, List<Users> users) {
//			super();
//			this.roleId = roleId;
//			this.userRole = userRole;
//			this.users = users;
//		}
//		UserRoles role1 = new UserRoles(1, "Employee");
//		UserRoles role2 = new UserRoles(2, "Financial Manager");
		
		public UserRoles(int roleId) {
			
			switch (roleId) {
			case 1:			
				this.roleId = roleId;
				this.userRole = "Employee";
				break;
			case 2:			
				this.roleId = roleId;
				this.userRole = "Finance Manager";
				break;
			}
		}

		public UserRoles(int roleId, String userRole) {
			super();
			this.roleId = roleId;
			this.userRole = userRole;
		}

		public String getUserRole() {
			return userRole;
		}

		public int getRoleId() {
			return roleId;
		}

		@Override
		public String toString() {
			return "UserRoles [roleId=" + roleId + ", userRole=" + userRole + "]";
		}

		
//		@Override
//		public String toString() {
//			return "UserRoles [roleId=" + roleId + ", userRole=" + userRole + ", users=" + users + "]";
//		}
		
		
		
}
