package com.project1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "reimbstatus")

public class ReimbStatus {
	
	@Id
	@Column(name="statusid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int statusId;
	
	@Column(name="reimbstatus")
	private String reimbStatus;

//	@OneToMany(mappedBy="Reimbursements")
//	private List<Reimbursements> reimb;
	
	public ReimbStatus() {
		// TODO Auto-generated constructor stub
	}
	
		public ReimbStatus(int statusId) {
		super();
		switch (statusId) {
		case 1:
			this.statusId = statusId;
			this.reimbStatus = "PENDING";
			break;
		case 2:
			this.statusId = statusId;
			this.reimbStatus = "APPROVED";
			break;
		case 3:
			this.statusId = statusId;
			this.reimbStatus = "DENIED";
			break;
		}
	}

		public ReimbStatus(int statusId, String reimbStatus) {
			super();
			this.statusId = statusId;
			this.reimbStatus = reimbStatus;
		}

		// populating the table with the following values as these entries are not going to be changed by the client
//		ReimbStatus pending = new ReimbStatus(1, "PENDING");
//		ReimbStatus approved = new ReimbStatus(2, "APPROVED");
//		ReimbStatus denied = new ReimbStatus(3, "DENIED");


		public int getStatusId() {
			return statusId;
		}

		public String getReimbStatus() {
			return reimbStatus;
		}
		
	
		@Override
		public String toString() {
			return "ReimbStatus [statusId=" + statusId + ", reimbStatus=" + reimbStatus + "]";
		}
		
		
		
}
