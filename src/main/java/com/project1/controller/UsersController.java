package com.project1.controller;

import org.apache.log4j.Logger;

import com.project1.MyLogger;
import com.project1.model.Users;
import com.project1.service.UsersService;

import io.javalin.http.Handler;

public class UsersController {
	private UsersService uServ;
	public static final Logger log = Logger.getLogger(UsersController.class);

	public UsersController(UsersService uServ) {
		super();
		this.uServ = uServ;
	}

	public Handler login = (ctx) -> {

		if (ctx.formParam("username") == null || ctx.formParam("password") == null) {
			ctx.redirect("/html/failedlogin.html");
		}
		Users user = uServ.passwordVerify(ctx.formParam("username"), ctx.formParam("password"));
		log.info(user.toString());;
		if (user != null) {

			ctx.sessionAttribute("user", user);

			if (user.getUserRole().getRoleId() == 1) {
				ctx.redirect("/html/employee.html");
			}
			if (user.getUserRole().getRoleId() == 2) {
				ctx.redirect("/html/financemanager.html");
			}

		} else {
			ctx.result("Incorrect username or password");
			ctx.redirect("/html/failedlogin.html");

		}
	};

	public Handler getSessUser = (ctx) -> {
		// only get session if there is not already one
		if (ctx.sessionAttribute("user") != null) {

			Users user = (Users) ctx.sessionAttribute("user");
			ctx.json(user);
		} else {
			ctx.redirect("/html/index.html");
		}

	};

	public Handler newUser = (ctx) -> {

		uServ.insertUser(ctx.formParam("username"), ctx.formParam("password"), ctx.formParam("firstname"),
				ctx.formParam("lastname"), ctx.formParam("email"), 1);
		ctx.redirect("/html/index.html");
	};

	public Handler logout = (ctx) -> {
		ctx.redirect("/html/index.html");
		ctx.sessionAttribute("user", null);
	};

	public UsersController() {
		// TODO Auto-generated constructor stub
	}

}
