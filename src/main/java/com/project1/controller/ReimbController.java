package com.project1.controller;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;

import com.project1.MyLogger;
import com.project1.dao.ReimbDao;
import com.project1.model.ReimbStatus;
import com.project1.model.ReimbType;
import com.project1.model.Reimbursements;
import com.project1.model.Users;
import com.project1.service.ReimbService;

import io.javalin.http.Handler;

public class ReimbController {

	private ReimbService rServ;

	public ReimbController(ReimbService rServ) {
		super();
		this.rServ = rServ;
	}
	public final static Logger log = Logger.getLogger(ReimbController.class);
	public ReimbController() {
		// TODO Auto-generated constructor stub
	}

	public Handler postReimb = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
		// parseInt is throwing an exception java.lang.NumberformatException: null
		// however, the number is parsed just fine and the value makes it into the
		// database
		int typeId = Integer.parseInt(ctx.formParam("type"));
		String desc = ctx.formParam("description");
		double amount = Double.parseDouble(ctx.formParam("amount"));
		ReimbType type = new ReimbType(typeId);

		rServ.insertReimb(amount, desc, user, type);
		ctx.redirect("/html/reimbrequest.html");
		MyLogger myLog = new MyLogger();
	};
	public Handler getByAuthor = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
//		System.out.println(user.toString());
		List<Reimbursements> reimb = rServ.findByAuthor(user);
//		System.out.println(reimb.toString());

		ctx.json(reimb);
	};

	public Handler getByPending = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
//		System.out.println(user.toString());
		List<Reimbursements> reimb = rServ.findPending();
//		System.out.println(reimb.toString());

		ctx.json(reimb);
	};
	
	public Handler getByDeny = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
//		System.out.println(user.toString());
		List<Reimbursements> reimb = rServ.findDeny();
//		System.out.println(reimb.toString());
		ctx.redirect("/html/financemanager.html");
		ctx.json(reimb);
	};
	
	public Handler getByApproved = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
//		System.out.println(user.toString());
		List<Reimbursements> reimb = rServ.findApproved();
//		System.out.println(reimb.toString());
		ctx.redirect("/html/financemanager.html");
		ctx.json(reimb);
	};

	public Handler getAllReimb = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
//		System.out.println(user.toString());
		List<Reimbursements> reimb = rServ.getAll();
//		System.out.println(reimb.toString());

		ctx.json(reimb);
	};

	public Handler updateReimb = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
		int reimbId = Integer.parseInt(ctx.formParam("reimbid"));
		Timestamp resolved = Timestamp.valueOf(ctx.formParam("resolved"));
		int status = Integer.parseInt(ctx.formParam("status"));
//		System.out.println(user.toString());

		rServ.updateReimb(user, reimbId, resolved, status);

	};
	public Handler approve = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
		int reimbId = Integer.parseInt(ctx.formParam("reimbId"));
//		System.out.println(reimbId);
		Timestamp resolved = new Timestamp(System.currentTimeMillis());

		rServ.updateReimb(user, reimbId, resolved, 2);
		ctx.redirect("/html/financemanager.html");
	};

	public Handler deny = (ctx) -> {
		Users user = (Users) ctx.sessionAttribute("user");
		int reimbId = Integer.parseInt(ctx.formParam("reimbId"));
//		System.out.println(reimbId);
		Timestamp resolved = new Timestamp(System.currentTimeMillis());

		rServ.updateReimb(user, reimbId, resolved, 3);
		ctx.redirect("/html/financemanager.html");
	};

}

//	public Handler deny = (ctx) -> {
//		Users user = (Users) ctx.sessionAttribute("user");
////		int reimbId = Integer.parseInt(ctx.formParam("reimbId"));
////		System.out.println(reimbId);
//		Timestamp resolved = new Timestamp(System.currentTimeMillis());
//
////		rServ.updateReimb(user, reimbId, resolved, 3);
//		ctx.redirect("/html/financemanager.html");
//	};
//	public Handler findById = (ctx) -> {
//		Users user = (Users) ctx.sessionAttribute("user");
//		if (user != null) {
//			System.out.println(ctx.pathParamMap());
//			int reimbId = Integer.parseInt(ctx.pathParam("reimbId"));
//			Reimbursements reimb = rServ.findById(reimbId);
//			ctx.redirect("/html/reimbbyid.html");
//			ctx.json(reimb);
//		}
//	};