package com.project1.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	
	public static SessionFactory sf = new Configuration().configure("hibernate/hibernate.cfg.xml").buildSessionFactory();
	
	private Session sess;
	

	public Session getSession() {
		if(sess == null) {
			sess = sf.openSession();
		}
		return sess;
		
	}
	
	public Session currentSession() {
		sess = sf.getCurrentSession();
		return sess;
		
	}
	
	public void closeSes() {
		sess.close();
		sf.close();
	}

}
