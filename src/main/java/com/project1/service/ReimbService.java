package com.project1.service;

import java.sql.Blob;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.project1.dao.ReimbDao;
import com.project1.model.ReimbStatus;
import com.project1.model.ReimbType;
import com.project1.model.Reimbursements;
import com.project1.model.Users;

public class ReimbService {
	
	private ReimbDao rDao;
	
	public ReimbService() {
		super();
		this.rDao = rDao;
	}
	
	public ReimbService (ReimbDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	public Reimbursements findById (int id) {
		
		Reimbursements reimb = rDao.findById(id);
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
		
	}
	
	public void insertReimb(double amount, String description, Blob img, Users author, ReimbType typeHolder) {
		if (amount==0 || description == null) {
			throw new IllegalArgumentException();
		}
	}
	
	public void insertReimb(double amount, String description, Users user, ReimbType type) {
		if (amount==0 || description == null) {
			throw new IllegalArgumentException();
		}
		Reimbursements reimb = new Reimbursements(amount, description, user, type);
		
		rDao.insert(reimb);
		
	}

	public List<Reimbursements> findByStatus(int statusId) {
		List<Reimbursements> reimb = rDao.findByStatus(statusId);
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
	}
	
	public List<Reimbursements> findPending() {
		List<Reimbursements> reimb = rDao.findPending();
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
	}
	
	public List<Reimbursements> findDeny() {
		List<Reimbursements> reimb = rDao.findDeny();
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
	}
	public List<Reimbursements> findApproved() {
		List<Reimbursements> reimb = rDao.findApproved();
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
	}
	
	public List<Reimbursements> findByAuthor(Users user) {
		
		List<Reimbursements> reimb = rDao.findByAuthor(user);
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
	}
	
	public List<Reimbursements> getAll () {
		List<Reimbursements> reimb = rDao.getAll();
		if (reimb == null) {
			throw new NullPointerException();
		}
		
		return reimb;
	
	}

	public void updateReimb(Users user, int reimbId, Timestamp resolved, int status) {

		Reimbursements reimb = rDao.findById(reimbId);
		ReimbStatus holder = new ReimbStatus(status);
		reimb.setResolver(user);
		reimb.setResolved(resolved);
		reimb.setStatusHolder(holder);
		
		rDao.update(reimb);
		
	}



}
