package com.project1.service;

import java.util.List;

import com.project1.dao.UsersDao;
import com.project1.model.UserRoles;
import com.project1.model.Users;
import com.project1.util.HibernateUtil;

public class UsersService {
	
	
	
	private UsersDao userDao;

	public UsersService() {
		super();
		this.userDao = userDao;
	}
	
	public UsersService (UsersDao userDao) {
		super();
		this.userDao = userDao;
	}

	public Users getUserById (int id) {

		Users user = userDao.findById(id);

		if (user == null) {
			throw new NullPointerException();
		}
		
		return user;
	}
	
	public Users getByUsername (String username) {
		
		Users user = userDao.findByUsername(username);
		if (user==null) {
			throw new NullPointerException();
		}
		return user;
	}
	

	public List<Users> getUserByFirstName (String firstname) {
		List<Users> user = userDao.findByFirstname(firstname);
		if (user == null) {
			throw new NullPointerException();
		}
		
		return user;
	}
	
	public List<Users> getUserByLastName (String lastname) {
		List<Users> user = userDao.findByFirstname(lastname);
		if (user == null) {
			throw new NullPointerException();
		}
		
		return user;
	}

	public List<Users> getUserByEmail (String email) {
		List<Users> user = userDao.findByFirstname(email);
		if (user == null) {
			throw new NullPointerException();
		}
		
		return user;
	}
	
	public List<Users> getUserByRole (int userRole) {
		List<Users> user = userDao.findByUserRole(userRole);
		if (user == null) {
			throw new NullPointerException();
		}
		
		return user;
	}
	
	public Users passwordVerify (String username, String password) {
		
		Users user = userDao.findByUsername(username);
		if (user.getPassword().equals(password)) {
			
			return user;
		}
		return null;
	}
	
	public void insertUser (String username, String password, String firstname, String lastname, String email, int roleId) {
		if (userDao.findByUsername(username) != null) {
			throw new IllegalArgumentException();
		}
		Users user = new Users(username, password, firstname, lastname, email, roleId);
		
		userDao.insertUser(user);
	}
	
	public void updateUserPassword (String username, String password) {
		if (userDao.findByUsername(username) == null) {
			throw new NullPointerException();
		} 
		Users user = userDao.findByUsername(username);
		user = new Users(user.getUsername(), password, user.getFirstname(), user.getLastname(), user.getEmail(), user.getUserRole().getRoleId());
		userDao.updateUser(user);
	}

	public void updateUserName (String username, String firstname, String lastname) {
		if (userDao.findByUsername(username) == null) {
			throw new NullPointerException();
		} 
		Users user = userDao.findByUsername(username);
		user = new Users(username, user.getPassword(), firstname, lastname, user.getEmail(), user.getUserRole().getRoleId());
		userDao.updateUser(user);
	}

	public void closeSes() {
		userDao.closeSes();
		
		
	}

}
