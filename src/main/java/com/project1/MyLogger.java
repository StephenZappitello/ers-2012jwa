package com.project1;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class MyLogger {

	public static final Logger log = Logger.getLogger(MyLogger.class.getName());

	public static void main(String[] args) throws IOException, SQLException {
		
		log.info("User attempted a login");
	
	}

}
