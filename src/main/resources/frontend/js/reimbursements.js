

let reimbBody = document.querySelector("reimbTable > tbody");

function loadReimb() {
    let xhttp = new XMLHttpRequest();

    xhttp.open("post", "/reimb/author");
    xhttp.onload = () => {
        xhttp.onreadystatechange = function() {

            if (this.readyState == 4 && xhttp.status == 200) {
                try {
                    let json = JSON.parse(request.responseText);
                    popReimb(json);
        
                } catch (e) {
                    console.log("Could not load reimbursements.")
                }
    
            }
        }
    }
    xhttp.send();
}

function popReimb(json) {
    const tr = document.createElement("tr");
    json.forEach(row => {
        row.forEach((cell) => {
            const td = document.createElement("td");
            td.textContent = cell;
            tr.appendChild(td);
        });
        reimbBody.appendChild(tr);
    });

}

document.addEventListener("DOMContentLoaded", () => { loadReimb() });